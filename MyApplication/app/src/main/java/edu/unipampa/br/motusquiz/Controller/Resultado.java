package edu.unipampa.br.motusquiz.Controller;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import edu.unipampa.br.motusquiz.Model.Questions;
import edu.unipampa.br.motusquiz.R;

public class Resultado extends AppCompatActivity {
    Bundle bundle;
    TextView textoSelecionado, tituloResultado;

    Questions q = new Questions();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        bundle = getIntent().getExtras();
        int resposta = bundle.getInt("respostaTerceiraPergunta-Final");


        textoSelecionado = (TextView) findViewById(R.id.textoSelecionado);


        tituloResultado = (TextView) findViewById(R.id.resultado);
        tituloResultado.setText("Sua poesia é:");
        Typeface font1 = Typeface.createFromAsset(getAssets(), "fonts/Georgina.ttf");
        tituloResultado.setTypeface(font1);


        if (resposta == 3) {

            Typeface res = Typeface.createFromAsset(getAssets(), "fonts/Georgina.ttf");
            textoSelecionado.setTypeface(res);
            textoSelecionado.setText(q.getR8());

        } else if (resposta > 3 && resposta <= 4) {

            Typeface res = Typeface.createFromAsset(getAssets(), "fonts/Georgina.ttf");
            textoSelecionado.setTypeface(res);
            textoSelecionado.setText(q.getR2());

        } else if (resposta > 4 && resposta <= 5) {

            Typeface res = Typeface.createFromAsset(getAssets(), "fonts/Georgina.ttf");
            textoSelecionado.setTypeface(res);
            textoSelecionado.setText(q.getR3());


        } else if (resposta > 5 && resposta <= 6) {

            Typeface res = Typeface.createFromAsset(getAssets(), "fonts/Georgina.ttf");
            textoSelecionado.setTypeface(res);
            textoSelecionado.setText(q.getR4());


        } else if (resposta > 6 && resposta <= 7) {

            Typeface res = Typeface.createFromAsset(getAssets(), "fonts/Georgina.ttf");
            textoSelecionado.setTypeface(res);
            textoSelecionado.setText(q.getR5());


        } else if (resposta > 7 && resposta <=8) {

            Typeface res = Typeface.createFromAsset(getAssets(), "fonts/Georgina.ttf");
            textoSelecionado.setTypeface(res);
            textoSelecionado.setText(q.getR6());


        }else if (resposta > 8 && resposta <= 9) {

            Typeface res = Typeface.createFromAsset(getAssets(), "fonts/Georgina.ttf");
            textoSelecionado.setTypeface(res);
            textoSelecionado.setText(q.getR7());


        }else if (resposta > 9 && resposta <= 12) {

            Typeface res = Typeface.createFromAsset(getAssets(), "fonts/Georgina.ttf");
            textoSelecionado.setTypeface(res);
            textoSelecionado.setText(q.getR1());


        }else if (resposta > 12 && resposta <= 14) {

            Typeface res = Typeface.createFromAsset(getAssets(), "fonts/Georgina.ttf");
            textoSelecionado.setTypeface(res);
            textoSelecionado.setText(q.getR9());


        } else  {

            Typeface res = Typeface.createFromAsset(getAssets(), "fonts/Georgina.ttf");
            textoSelecionado.setTypeface(res);
            textoSelecionado.setText(q.getR10());


        }

    }
}
