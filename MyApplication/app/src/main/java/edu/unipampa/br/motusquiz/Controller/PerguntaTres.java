package edu.unipampa.br.motusquiz.Controller;;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import edu.unipampa.br.motusquiz.Model.Questions;
import edu.unipampa.br.motusquiz.R;

public class PerguntaTres extends AppCompatActivity {
    Bundle bundle;
    int resultado2, resultado_aux;
    TextView pergunta3, rodape;
    ImageButton ok;
    int points;
    RadioGroup rg3;
    RadioButton rb1,rb2,rb3, rb4, rb5;

    Questions questions = new Questions();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pergunta_tres);


        bundle = getIntent().getExtras();

        rb1 = (RadioButton) findViewById(R.id.radioButtonP3);
        Typeface fontRb1 = Typeface.createFromAsset(getAssets(), "fonts/Quicksand_Book.otf");
        rb1.setTypeface(fontRb1);
        rb1.setText(questions.getOpcao1Pergunta3());


        rb2 = (RadioButton) findViewById(R.id.radioButton2P3);
        Typeface fontRb2 = Typeface.createFromAsset(getAssets(), "fonts/Quicksand_Book.otf");
        rb2.setTypeface(fontRb2);
        rb2.setText(questions.getOpcao2Pergunta3());

        rb3 = (RadioButton) findViewById(R.id.radioButton3P3);
        Typeface fontRb3 = Typeface.createFromAsset(getAssets(), "fonts/Quicksand_Book.otf");
        rb3.setTypeface(fontRb3);
        rb3.setText(questions.getOpcao3Pergunta3());


        rb4 = (RadioButton) findViewById(R.id.radioButton4P3);
        Typeface fontRb4 = Typeface.createFromAsset(getAssets(), "fonts/Quicksand_Book.otf");
        rb4.setTypeface(fontRb4);
        rb4.setText(questions.getOpcao4Pergunta3());


        rb5 = (RadioButton) findViewById(R.id.radioButton5P3);
        Typeface fontRb5 = Typeface.createFromAsset(getAssets(), "fonts/Quicksand_Book.otf");
        rb5.setTypeface(fontRb3);
        rb5.setText(questions.getOpcao5Pergunta3());

        pergunta3 = (TextView) findViewById(R.id.textViewPergunta3);
        Typeface p3 = Typeface.createFromAsset(getAssets(), "fonts/Georgina.ttf");
        pergunta3.setTypeface(p3);
        pergunta3.setText(questions.getP3());

        rodape = (TextView) findViewById(R.id.rodapeText3);
        Typeface fontRodape = Typeface.createFromAsset(getAssets(), "fonts/Georgina.ttf");
        rodape.setTypeface(fontRodape);
        rodape.setText("Motus - Programa C");

        rg3 = (RadioGroup) findViewById(R.id.rg3);
        rg3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int i) {
                switch (i) {

                    case R.id.radioButtonP3:
                        points = 1;
                        questions.setResultado(points);
                        break;
                    case R.id.radioButton2P3:
                        points = 2;
                        questions.setResultado(points);
                        break;
                    case R.id.radioButton3P3:
                        points = 3;
                        questions.setResultado(points);
                        break;
                    case R.id.radioButton4P3:
                        points = 4;
                        questions.setResultado(points);
                        break;
                    case R.id.radioButton5P3:
                        points = 5;
                        questions.setResultado(points);
                        break;
                }

            }
        });


        /**/

    }

    private void exibirMensagem(String mensagem){
        Context contexto = getApplicationContext();
        int duracao = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(contexto, mensagem, duracao);
        toast.show();
    }

    public void enviarDados(Intent it) {
        resultado2 = bundle.getInt("respostaSegundaPergunta");
        resultado_aux = questions.getResultado() + resultado2;
        it.putExtra("respostaTerceiraPergunta-Final", resultado_aux);

    }

    public void buttonVoltarPara2(View v){
        Intent i = new Intent(PerguntaTres.this, PerguntaDois.class);
        startActivity(i);
    }

    public void irParaResultado(View v){
        if(rb1.isChecked()== false && rb2.isChecked()== false && rb3.isChecked() == false&& rb4.isChecked() == false&& rb5.isChecked() == false){
            String texto3 = "Selecione alguma opção!";
            exibirMensagem(texto3);
        }else{
            Intent i = new Intent(this, Resultado.class);
            enviarDados(i);
            startActivity(i);
        }
    }
}

